# Flask-Mongo

Create a Python Flask API 
1. PUT Operation: Create student information and generate a 
student ID for each new student. 
2. GET Operation: Retrieve information for a single student 
based on the student ID. 
3. GET Operation: Retrieve information for all students. 
4. PUT Operation: Update any student information based on 
the student ID provided. 
5. DELETE Operation: Delete student information. 
6. POST Operation: User login with user ID and password 
verification (this service can have hard-coded values for user 
ID and password). 
All student data must be stored in MongoDB. The student 
information should be in JSON format. 
